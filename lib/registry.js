/*jshint proto:true*/
'use strict';

var path = require('path');
var assert = require('assert');
var express = require('express');
var debug = require('debuglog')('enrouten/registry');
var propertyExists = Function.prototype.call.bind(Object.prototype.hasOwnProperty);

function endsWith(str, chars) {
    return str.slice(-chars.length) === chars;
}


function removeSuffix(str, chars) {
    return endsWith(str, chars) ? str.slice(0, -chars.length) : str;
}

module.exports = function create(mountpath, router, options) {
    var routes;
    var policy = require(options["policy"]);
    /**
     * Wrapper for default `express.Router` that will allow registration of
     * name routes.
     * @param options for the current route. Currently supports `path` and `name`.
     */
    function registry(options) {
        var path;

        options = options || {};
        options.name = options.name || undefined;
        options.path = options.path || '/';

        if (typeof options.name === 'string') {
            assert.ok(!propertyExists(registry.routes, options.name), 'A route already exists for the name "' + options.name + '"');

            // Do our best to normalize paths
            path = removeSuffix(registry._mountpath, '/');
            path += options.path;

            if (path !== '/') {
                path = removeSuffix(path, '/');
            }

            debug('Registering name', options.name, 'for path', path);
            registry.routes[options.name] = path;
        }

        var object = Object.getPrototypeOf(registry).route(options.path);

        return object;
    }

    // Support composability
    if (typeof router === 'function' && router.name === 'registry') {
        mountpath = removeSuffix(router._mountpath, '/') + mountpath;
        routes = router.routes;
        router = undefined;
    }

    mountpath = mountpath || '/';
    router = router || new express.Router(options);

    registry.routes = routes || Object.create(null);
    registry._mountpath = mountpath;
    registry.__proto__ = router;
    registry.policies = policy.policies;

    /**
    * @param controller {string} - Controller is the name for the controller file (it is the same name you used in your
     * policy.js file
     * @param path {string} - Path is the defined route path
    * */
    registry.routePolicies = function(controller, path) {
        //#TODO Maybe change this to be in the reverse order
        assert.ok(typeof controller === "string", "Controller must be a string");
        assert.ok(typeof path === "string", "Path must be a string");
        var _middleware = [];
        if (registry.policies[controller] !== undefined && typeof registry.policies[controller] == "Object") {
            _middleware = registry.policies[controller]["*"];
            _middleware = _middleware.concat(registry.policies[controller][path]);
        }
        return _middleware;
    };

    // Recursively search for the first non-registry (express) router.
    Object.defineProperty(registry, '_router', {
        get: function () {
            return router._router || router;
        }
    });

    return registry;

};
